package ch.ergon.edu.ci.mailinator.distribution;

import ch.ergon.edu.ci.mailinator.domain.Letter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class SignatureGenerator {

    public void insertSignature(Letter letter) {

        Path path = Paths.get("src/main/resources/signature.txt");

        List<String> lines;


        try {
            lines = Files.readAllLines(path);
            letter.setSignature(lines.get(0));
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

}
