package ch.ergon.edu.ci.mailinator.validation;

import ch.ergon.edu.ci.mailinator.domain.Letter;

import java.util.Arrays;
import java.util.List;

public class LetterValidator {
    private static final int MAX_ZAHLEN = 4;
    private static final List<String> VALID_CITIES = Arrays.asList(
            "Zürich", "Genf", "Basel", "Bern", "Lausanne", "Winterthur",
            "Luzern", "St. Gallen", "Lugano", "Biel/Bienne", "Thun", "Köniz",
            "La Chaux-de-Fonds", "Freiburg", "Schaffhausen", "Chur", "Vernier",
            "Neuenburg", "Uster", "Sitten");

    public LetterValidator() {
        System.out.println("Valid cities: " + VALID_CITIES);
    }

    public boolean isValid(Letter letter) {
        return (isPlzFormatCorrect(letter.getPlz())
                && isContentNotEmpty(letter.getContent())
                && isValidCity(letter.getCity()));
    }

    boolean isPlzFormatCorrect(int plz) {
        String plzS = Integer.toString(plz);
        return (plzS.length() == MAX_ZAHLEN);
    }

    boolean isContentNotEmpty(String content) {
        return !(content == null || content.isEmpty());
    }

    boolean isValidCity(String city) {
        return (VALID_CITIES.contains(city));
    }

}
