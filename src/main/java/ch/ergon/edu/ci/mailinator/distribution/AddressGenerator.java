package ch.ergon.edu.ci.mailinator.distribution;

import ch.ergon.edu.ci.mailinator.domain.AddressEntry;
import ch.ergon.edu.ci.mailinator.domain.Letter;

public final class AddressGenerator {

    private AddressGenerator() {

    }


    private static final String SEPARATOR = " ";

    public static void insertAddress(Letter letter, AddressEntry addressEntry) {
        letter.setName(addressEntry.getFirstName() + SEPARATOR + addressEntry.getLastName());
        letter.setStreet(addressEntry.getStreetName() + SEPARATOR + addressEntry.getHouseNumber());
        letter.setCity(addressEntry.getCity());
        letter.setPlz(Integer.parseInt(addressEntry.getPlz()));
    }
}
