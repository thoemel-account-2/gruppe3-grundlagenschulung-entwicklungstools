package ch.ergon.edu.ci.mailinator.distribution;

import ch.ergon.edu.ci.mailinator.domain.Letter;

import java.util.Random;

public class ContentGenerator {


    public void insertContent(Letter letter) {
        String availableLetters = "abcdefghijklmnopqrstuvwxyz";

        Random randomLetters = new Random();

        StringBuffer content = new StringBuffer();

        final int contentCount = 100;

        for (int i = 0; i < contentCount; i++) {
            int position = randomLetters.nextInt(availableLetters.length());
            String nextLetter = availableLetters.substring(position, position + 1);
            content.append(nextLetter);
        }

        letter.setContent(content.toString());




    }
}
