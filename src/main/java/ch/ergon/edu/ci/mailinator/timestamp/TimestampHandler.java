package ch.ergon.edu.ci.mailinator.timestamp;

import ch.ergon.edu.ci.mailinator.domain.Letter;

public final class TimestampHandler {
    private TimestampHandler() {

    }

    public static void setStartProcessingTimestamp(Letter letter) {
        letter.setStartTimestamp(System.nanoTime());
    }

    public static void setEndProcessingTimestamp(Letter letter) {
        letter.setEndTimestamp(System.nanoTime());
    }
}
