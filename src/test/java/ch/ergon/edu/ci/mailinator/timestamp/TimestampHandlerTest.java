package ch.ergon.edu.ci.mailinator.timestamp;

import ch.ergon.edu.ci.mailinator.domain.Letter;
import org.junit.Assert;
import org.junit.Test;

public class TimestampHandlerTest {

    private static final int MAX_NANO_SEKUNDEN = 1000000000;
    @Test
    public void startTimestampGTZero() {
        Letter letter = new Letter();
        TimestampHandler.setStartProcessingTimestamp(letter);
        Assert.assertTrue(letter.getStartTimestamp() > 0);
    }

    @Test
    public void startTimestampSTtmp() {
        Letter letter = new Letter();
        TimestampHandler.setStartProcessingTimestamp(letter);
        long tmp = System.nanoTime();
        long tmp2 = tmp - letter.getStartTimestamp();
        Assert.assertTrue(tmp2 < MAX_NANO_SEKUNDEN);
    }

    @Test
    public void endTimestampGTZero() {
        Letter letter = new Letter();
        TimestampHandler.setEndProcessingTimestamp(letter);
        Assert.assertTrue(letter.getEndTimestamp() > 0);
    }

    @Test
    public void endTimestampSTtmp() {
        Letter letter = new Letter();
        TimestampHandler.setEndProcessingTimestamp(letter);
        long tmp = System.nanoTime();
        long tmp2 = tmp - letter.getEndTimestamp();
        Assert.assertTrue(tmp2 < MAX_NANO_SEKUNDEN);
    }
}
